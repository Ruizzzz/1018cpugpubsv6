#include <stdio.h>
//#include "src_main_java_operator_Detector.h"
#include "Detector.h"
#include "detection.h"
#include <string>
#include <iostream>

using namespace std;

//global, label\datacfg 20191017 
char* _datacfg = NULL;
char** names = NULL;

char* labelpaths = NULL;
//change type char* into jstring
jstring stoJstring(JNIEnv* env, char* pat)
{
    jclass strClass = env->FindClass("Ljava/lang/String;");
    jmethodID ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = env->NewByteArray(strlen(pat));
    env->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*)pat);
    jstring encoding = env->NewStringUTF("utf-8");
    return (jstring)env->NewObject(strClass, ctorID, bytes, encoding);
}


//jstring to char*
char* jstring2char(JNIEnv* env, jstring gpuid)
{
	  char* rtn = NULL;
	  jclass clsstring = env->FindClass("java/lang/String");
	  jstring strencode = env->NewStringUTF("utf-8");
	  jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	  jbyteArray barr= (jbyteArray)env->CallObjectMethod(gpuid, mid, strencode);
	  jsize alen = env->GetArrayLength(barr);
	  jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
	  if (alen > 0)
	  {
	    rtn = (char*)malloc(alen + 1);
	    memcpy(rtn, ba, alen);
	    rtn[alen] = 0;
	  }
	  env->ReleaseByteArrayElements(barr, ba, 0);
	  return rtn; 
}



//JNIEXPORT jlong JNICALL Java_src_main_java_operator_Detector_initialize// 3-1: initialize(), return (jlong)peer.
JNIEXPORT jlong JNICALL Java_Detector_initialize// 3-1: initialize(), return (jlong)peer.
  (JNIEnv *env , jobject obg , jstring cfgfile, jstring weightfile, jstring datacfg, jstring labelpath, jint batchsize , jint gpu_index , jstring gpuid){
        //env是jni对应的一个对象
        //printf("************************************************ This is the 1st GetStringUTFChars...\n");
        cout<<"initialize"<<endl;
        //printf("before ************************************************ cfgfile: %s \n", cfgfile);
        //get获取对应的值
        char* _cfgfile = (char*)(env)->GetStringUTFChars(  cfgfile, 0 );
	//printf("after  ************************************************ cfgfile: %s \n", _cfgfile);

        //printf("************************************************ This is the 2nd GetStringUTFChars...\n");
        char* _weightfile = (char*)(env)->GetStringUTFChars( weightfile, 0 );

	//printf("Java_Detector_initialize in cpp.\n");
	//jstring 转化为 char*
        char* gpuids =jstring2char(env, gpuid);


    //加载网络文件，配置批处理的大小,返回网络net
	network* peer = load_network_test(_cfgfile, _weightfile , batchsize , gpu_index , gpuids);

	cout<<(jlong)peer<<endl;

    //调用ReleaseStringUTFChars()，意味着native方法不再使用该UTF-8字符串，这样被该UTF-8字符串占用的内存就可以被释放了
	(env)->ReleaseStringUTFChars(  cfgfile, _cfgfile );
	(env)->ReleaseStringUTFChars(  weightfile, _weightfile );

	// label\datacfg 20191017
	//获取.data 文件,提取names
	_datacfg = (char*)(env)->GetStringUTFChars( datacfg, 0 );
	cout<<"get names"<<endl;
	list *options = read_data_cfg(_datacfg);
        char *name_list = option_find_str(options, "names", 0);
        if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
        names = get_labels(name_list);
//	names = load_names(_datacfg);//names是一个char数组
	for(int sss=0;sss<10;sss++){
	cout<<names[sss]<<" ,";
	}
	cout<<endl;
        //jstring转化为char
        labelpaths =jstring2char(env, labelpath);

    cout<<"initialize completed！"<<endl;
	return (jlong)peer;
}





//jpg转byte
//JNIEXPORT jbyteArray JNICALL Java_src_main_java_operator_Detector_jpg2Bytes
JNIEXPORT jbyteArray JNICALL Java_Detector_jpg2Bytes
  (JNIEnv *env, jobject obj, jstring path, jint w, jint h, jint c){
	
      //printf("************************************************ This is the 3rd GetStringUTFChars...\n");
      char* _path = (char*)(env)->GetStringUTFChars(  path, 0 );
      unsigned char * bytes = jpg2BytesInC(_path, c);
      //printf("Detector.cpp bytes[1] %d.\n", bytes[1]);

	//C++中的BYTE[]转jbyteArray
	//nOutSize是BYTE数组的长度  BYTE pData[]
         //int nOutSize = sizeof(bytes)/sizeof(unsigned char);
         int nOutSize = w * h * c;

	//printf("size of bytes: %ld.\n",sizeof(bytes));
       // printf("size of unsigned char: %ld.\n", sizeof(unsigned char));	
	//printf("whc, nOutSize: %d.\n", nOutSize);	

	jbyte *by = (jbyte*)bytes;
	jbyteArray jarray = (env)->NewByteArray(nOutSize); 
	(env)->SetByteArrayRegion(jarray, 0, nOutSize, by);

      //printf("jarray[0] %c.\n", jarray[0]);
      return jarray;

}




//JNIEXPORT jobjectArray JNICALL Java_Detector_computeBoxesAndAccByInputBytes
//  (JNIEnv *, jobject, jlong, jstring, jbyteArray, jstring, jfloat, jfloat, jint, jint, jint, jint);

//JNIEXPORT jobjectArray JNICALL Java_src_main_java_operator_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
//JNIEXPORT jobjectArray JNICALL Java_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
//  (JNIEnv *env, jobject obj, jlong structWrapperPeer, jbyteArray bytes, jstring outfile,jfloat thresh,jfloat hier_thresh,
//  jint fullscreen,jint ww, jint hh, jint c)computeBoxesAndAccByInputBytes
  JNIEXPORT jstring JNICALL Java_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
    (JNIEnv *env, jobject obj, jlong structWrapperPeer, jbyteArray bytes, jstring outfile,jfloat thresh,jfloat hier_thresh,
    jint fullscreen,jint ww, jint hh, jint c)
  {

        //printf("computeBoxesAndAccByInputBytes in cpp.\n");
    //net 代表神经网络
	network* net = (network*)structWrapperPeer;

	// bs setting...
	set_batch_network(net, 1);

	//printf("************************************************ This is the 4th GetStringUTFChars...\n");

	//printf("before ************************************************ outfile:  %s\n", outfile);
    //get 获取outfile的值
  	char* _outfile = (char*)(env)->GetStringUTFChars( outfile, 0 );

	//printf("after  ************************************************ outfile:  %s\n", _outfile);



	//printf("************************************************ This is the 5th GetStringUTFChars...\n");



   	//jxxxxArray：各种类型的数组jArray
//  	jobjectArray infos = NULL;
//  	jstring info;
	jsize len;//数组大小
	int i;


	//float * bufferFloat = (float*) malloc(sizeof(float)*w*h*c);
	//jfloat * _buffer = (jfloat*) (env)->GetFloatArrayElements(buffer,NULL);

	//unsigned char * _bytes = (unsigned char*)bytes;

    /**------JNI jbyteArray转char------------*/
    //bytes转化为_bytes ,代表图片数据
	    unsigned char *_bytes = NULL;
	    jbyte *bytess = (env)->GetByteArrayElements(bytes, 0);
        int chars_len = (env)->GetArrayLength(bytes);
	    _bytes = new unsigned char[chars_len + 1];
	    memset(_bytes,0,chars_len + 1);
	    memcpy(_bytes, bytess, chars_len);
	    _bytes[chars_len] = 0;

	//JNI jstring转char*   方法1



          //printf("label path rtn:  %s\n", rtn);

	//另一种方法  方法2
       // char *nativeString = (env)->GetStringUTFChars(env, labelpath);
       //char *nativeString = env->GetStringUTFChars(labelpath, JNI_FALSE);

      // use your string

        //printf("before detectByInputBytes in cpp.\n");
       //names是初始化时提到的一个char数组并赋值
//       cout<<"detect beginning C"<<endl;

	char * Result =  detectByInputBytes1111(_bytes, thresh, 0.5, _outfile, fullscreen, names, net, ww, hh, c, labelpaths,_datacfg,5);
//    cout<<Result<<endl;
//	//char* bian jstring
	jstring info = stoJstring(env,Result);
//	cout<<info<<endl;
	(env)->ReleaseByteArrayElements(bytes, bytess, 0);
return info;
}

