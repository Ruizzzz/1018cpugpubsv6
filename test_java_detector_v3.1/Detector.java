//package operator;

import java.io.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
 
import javax.imageio.ImageIO;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.imageio.stream.FileImageInputStream;


import java.math.BigDecimal;

import java.util.Arrays;


class Box {
	float x;
	float y;
	float w;
	float h;
}
class BoxesAndAcc{
    float acc;
    Box  boxes;
    String names;
    boolean isVaild;
    int size;
}
public class Detector {

    private long peer;

    public long getPeer() {
	return peer;
    }
    
    // detect by input bytes...
//    public BoxesAndAcc[] execComputeBoxesAndAccByInputBytes(long strutWrapperPeer, byte[] bytes, String outfile, float thresh, float hier_thresh, int fullscreen, int w, int h, int c) {
//        return computeBoxesAndAccByInputBytes(strutWrapperPeer, bytes, outfile, thresh, hier_thresh, fullscreen, w,h,c);
//    }
	public String execComputeBoxesAndAccByInputBytes(long strutWrapperPeer, byte[] bytes, String outfile, float thresh, float hier_thresh, int fullscreen, int w, int h, int c) {
		return computeBoxesAndAccByInputBytes(strutWrapperPeer, bytes, outfile, thresh, hier_thresh, fullscreen, w,h,c);
	}

    public byte[] jpgfile2bytes(String path, int w, int h, int c) {
    	System.out.println("path: " + path + " " + c);
    	return jpg2Bytes(path, w, h , c);
    }
    
    public Detector(String cfgfile, String weightfile,String datacfg,  String labelpath, int batchsize, int gpu_index, String gpuid){	
	peer = initialize(cfgfile, weightfile,  datacfg,   labelpath, batchsize, gpu_index, gpuid);
    }	    
    
    /**
     * i 为 valArr下标
     * b 为次数
     * c 为a 下标
     */
    public byte[] getCopyArray(byte[] valArr, int t){
        byte[] a=null;//定义一个空数组
        if(null!=valArr){
             a=new byte[valArr.length*t];//生成多少次
            for(int i=0,b=0,c=0;i<=valArr.length && b<t && c<valArr.length*t;i++,c++){
                if(i>valArr.length-1){
                    i=0;
                    b++;
                }
                a[c]=valArr[i];
            }
        }
        return a ;
    }      
		
    private native byte[] jpg2Bytes(String path, int w, int h, int c);
	
//    private native BoxesAndAcc[] computeBoxesAndAccByInputBytes(long strutWrapperPeer, byte[] bytes, String outfile, float thresh, float hier_thresh, int fullscreen,int w, int h, int c);
    private native String computeBoxesAndAccByInputBytes(long strutWrapperPeer, byte[] bytes, String outfile, float thresh, float hier_thresh, int fullscreen,int w, int h, int c);

    private native long initialize(String cfgfile, String weightfile, String datacfg, String labelpath, int batchsize, int gpu_index, String gpuid );

    static {
        //System.load("/home/kfch/Downloads/0924/0925/Detector-v6/libjdetection.so"); 
	System.load("/usr/lib/mlr/libjdetect.so");
	//System.loadlibrary("libjdetection"); 
    }
    
    public static void main(String[] args) throws Exception {

	int w=640;//宽度
	int h=480;//高度
	int c=3;//通道数
	int bs =1;// for default...batchsize
	int gpu_index = 0; // Gpu for default...
	// darknet对于每个GPU都维护着同一个网络network，每个network通过gpu_index进行区分
	String gpuid = "0,1";

	int argsLength = args.length;

String cfgPath = "/root/map123/mlr/darknet-yolov3/cfg/resnet50.cfg";
// String cfgPath = "/root/map123/mlr/darknet-yolov3/cfg/yolov3.cfg";
      if(argsLength>=1){
      cfgPath = args[0];
   }

// String weightsPath = "./yolov3.weights";
//    String weightsPath = "/root/map123/app/app1/yolo/yolov3.weights";
      String weightsPath = "/root/map123/mlr/darknet-yolov3/resnet50.weights";
   if(argsLength>=2){
      weightsPath = args[1];
   }


// String datacfg = "./cfg/coco.data";
//    String datacfg = "/root/map123/app/app1/yolo/coco.data";
      String datacfg = "/root/map123/mlr/darknet/cfg/imagenet1k.data";
   if(argsLength>=3){
      datacfg = args[2];
   }


//    String labelpath = "/root/map123/app/app1/yolo/labels";
   String labelpath = "/root/map123/mlr/darknet/data/labels";
   if(argsLength>=4){
      labelpath = args[3];
   }

      String path = "/root/map123/mlr/darknet-yolov3/data/dog.jpg";

	if(argsLength>=5){
		path = args[4];
	}

	if(argsLength>=6){
		bs= Integer.parseInt(args[5]);
	}
	
	if(argsLength>=7){
			gpu_index= Integer.parseInt(args[6]);
		}
	
	if(argsLength>=8){
			gpuid= args[7];
		}
		System.out.println("load network");
	Detector obj = new Detector(cfgPath, weightsPath,datacfg, labelpath,bs, gpu_index, gpuid);
		System.out.println("load network over");
//		Image是一个抽象列，BufferedImage是Image的实现
//		Image和BufferedImage的主要作用就是将一副图片加载到内存中
		BufferedImage sourceImg =ImageIO.read(new FileInputStream(path));
	//System.out.println(String.format("%.1f",picture.length()/1024.0));// 源图大小 
	//System.out.println(sourceImg.getWidth()); // 源图宽度 
	//System.out.println(sourceImg.getHeight()); // 源图高度

	w = sourceImg.getWidth() ;//获取图片的宽度和长度
	h = sourceImg.getHeight();
	c = 3;
//jpg 转化为 byte[],输出path路径
//		字节串bytes,字节串也叫字节序列，是不可变的序列
	byte[] jpgbytes = obj.jpgfile2bytes(path, w, h,  c);
	//System.out.println("jpgbytes: " + (jpgbytes[1]<0?(256 + jpgbytes[1]):(jpgbytes[1])));
	//System.out.println("jpgbytes len: " + jpgbytes.length);
	
	//简单的拼接一个buffer...//新的byte数组，内容是jpgbytes的n倍
	byte[] jpgbytes_bs = new byte[w*h*c*(bs+2)];
	jpgbytes_bs = obj.getCopyArray(jpgbytes, bs + 1);
	 	
	
	int count=0;
    long sumtime = 0;
        
	while(count< bs){
		System.out.println("开");
		System.out.println("This is the " + (count + 1) + " pic of batchsize = " + bs + ".");
		
		count+=1;
               
		/*获取当前系统时间*/
   		 long startTime =  System.currentTimeMillis();

	       //System.out.println("#02: Begin compute...");
		//BoxesAndAcc[] boxesAndAccs = obj.execComputeBoxesAndAccByInputBuffer(obj.getPeer(),"./cfg/coco.data",  aaa,"predictions",(float)0.5, (float)0.5, 1,w,h,c);// from jpg
		//BoxesAndAcc[] boxesAndAccs = obj.execComputeBoxesAndAccByInputBuffer(obj.getPeer(),"./cfg/coco.data",  buffer1,"predictions",(float)0.5, (float)0.5, 1,w,h,c);// from txt

		// 通过输入的图片来预测。。。
		//BoxesAndAcc[] boxesAndAccs = obj.execComputeBoxesAndAcc(obj.getPeer(),"./cfg/coco.data",  path ,"predictions",(float)0.5, (float)0.5, 1, w,h,c);// from jpg

		System.out.println("predict beginning");
        // 通过输入的Bytes来预测。。。//
//		copyOfRange():从下标from开始赋值，赋to-from个值
   		byte[] jpgbytes_bs_part = Arrays.copyOfRange(jpgbytes_bs, w*h*c*count, w*h*c*(count + 1));

		/** 主要的计算部分*/
//		BoxesAndAcc[] boxesAndAccs = obj.execComputeBoxesAndAccByInputBytes(obj.getPeer(), jpgbytes_bs_part  ,"predictions",(float)0.5, (float)0.5, 1, w,h,c);// from jpgbytes
		for(int s=0;s<2000;s++){
			String result = obj.execComputeBoxesAndAccByInputBytes(obj.getPeer(), jpgbytes_bs_part  ,"predictions",(float)0.5, (float)0.5, 1, w,h,c);// from jpgbytes

			//获取当前的系统时间，与初始时间相减就是程序运行的毫秒数，除以1000就是秒数
			long endTime =  System.currentTimeMillis();
			long usedTime = (endTime-startTime);
			sumtime += usedTime;

			System.out.println("names:"+result);

		}

//		if(boxesAndAccs==null){
//			System.out.println("No Boxes");
//		}
//		for(BoxesAndAcc boxesAndAcc : boxesAndAccs){
//
//			if(boxesAndAcc.isVaild==true){
//				System.out.println("names:"+boxesAndAcc.names);
//				System.out.println("acc:"+boxesAndAcc.acc);
//				System.out.println("x:"+boxesAndAcc.boxes.x);
//				System.out.println("y:"+boxesAndAcc.boxes.y);
//				System.out.println("w:"+boxesAndAcc.boxes.w);
//				System.out.println("h:"+boxesAndAcc.boxes.h);
//				Box b = boxesAndAcc.boxes;
//		              /*  int left  = (b.x-b.w/2.)*im.w;
//		                int right = (b.x+b.w/2.)*im.w;
//		                int top   = (b.y-b.h/2.)*im.h;
//		                int bot   = (b.y+b.h/2.)*im.h;*/
//
//				System.out.println("-------------------");
//
//			}
//		}// for
	 }// while	
	System.out.println("-------------------" + " batchsize = " + bs + ", total time: " + sumtime + " micro-secs.");
    }//main
} //class
