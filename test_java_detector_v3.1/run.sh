#!/usr/bin/env bash
#javac Detector.java
#javah Detector
#export LD_LIBRARY_PATH=/home/kfch/Downloads/0924/0925/test_java_detector_v3.1
export LD_LIBRARY_PATH=/usr/lib/mlr:$LD_LIBRARY_PATH
#g++ -I./ -I=/opt/jvm/jdk1.8.0_201/include -I=opt/jvm/jdk1.8.0_201/include/linux -L=/opt/jvm/jdk1.8.0_201/lib -fPIC -shared -o libjdetection.so Detector.cpp -L/home/kfch/Downloads/0404Java/Detection-v3/test_java_detector_v3.1 -L/usr/local/cuda-10.0/lib64  -ldetection
#g++ -I./ -I=/opt/jdk1.8.0_201/include -L=/opt/jdk1.8.0_201/lib -fPIC -shared -o libjdetection.so Detector.cpp -L/home/chenkk/Files/Others/Detection-v3/test_java_detector_v3.1 -L/usr/local/cuda/lib64  -ldetection
#g++ -I./ -I=/opt/jre/jdk1.8.0_201/include -I=opt/jre/jdk1.8.0_201/include/linux -L=/opt/jre/jdk1.8.0_201/lib -fPIC -shared -o libjdetection.so Detector.cpp -L/home/kfch/Downloads/0924/1010CPUGPUV6/test_java_detector_v3.1 -L/usr/local/cuda-10.0/lib64  -ldetection
g++-7 -I./ -I/usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -L/usr/lib/jvm/java-8-openjdk-amd64/lib -fPIC -shared -o libjdetect.so Detector.cpp -L/root/map123/mlr/1018cpugpubsv6/test_java_detector_v3.1 -L/usr/local/$v_cuda/lib64 -ldetect
#g++-7 -I./ -I/usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -fPIC -shared -o libjdetection.so runtime_detection_Detector.cpp -L/root/map123/app/app1/project2_dynamiclinklib/detection/1018CPUGPUBSV6/test_java_detector_v3.1 -L/usr/local/$v_cuda/lib64 -ldetection

#javac Detector.java
#java Detector ./cfg/yolov3.cfg ./yolov3.weights 768 576 3 /home/kfch/Downloads/0404Java/Detection-v3/test_java_detector_v3.1/dog_.txt
#cp ./libjdetect_cpu.so /usr/lib/mlr
cp ./libjdetect.so /usr/lib/mlr