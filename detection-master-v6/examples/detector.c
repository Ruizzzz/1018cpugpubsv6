#include "detection.h"
#include "cuda.h"
#include<stdio.h>

//char **names ;
//char *name_list;

//char**  load_names(char *datacfg){
//    list *options = read_data_cfg(datacfg);
//    char *name_list = option_find_str(options, "names", "data/names.list");
//    char **names = get_labels(name_list);
//    return names;
////}
char**  load_names(char *datacfg){
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", 0);
    if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
    char **names = get_labels(name_list);
    for(int sss=0;sss<10;sss++){
    	printf("%s",names[sss]);
    	}
//   char *xxx [ ] = {"China","French","America","German"};
//   names=xxx;
    return names;
}



network * load_network_test(char *cfgfile, char *weightfile,int batchsize, int gpu_index_params, char *gpuid)
{

    //printf("from java batchsize: %d\n", batchsize);
    //printf("from java gpuids: %s\n", gpuid);

    gpu_index = gpu_index_params;
    
    network *net = load_network(cfgfile, weightfile, 0);
    
    set_batch_network(net, 1);
    //set_batch_network(net, batchsize);
    //printf("actual net batchsize: %d\n", net->batch);

    return net;
}



//#define INT_MAX 2147483647 //[64位编译器int最大值]
#define E_MAX 255

//int * png_size(FILE *fp1)
//{	//得到PNG文件中二进制数据输出宽高（单位：像素）
//	rewind(fp1);			//文件重定向到开始
//	int  place = 0,index = 0,i;
//	char data;
//	int size[8];
//	int width = 0, height = 0;
//	while(place<=24)
//   {
//   		place++;
//   		data = fgetc(fp1);
//   		if(place>16){
//   			size[index] =(int) data;
//   			index++;
//		}
//   }
//   for(i=0;i<4;i++) if(size[i]<0) size[i] = (size[i]&INT_MAX)&E_MAX;
//   width = 	256*size[2]+size[3];
//   height = 256*size[6]+size[7];
//   printf("【尺寸】图片宽为%d，长为%d\n",width,height);
//   static int size_w_h[2] = {0,0};
//   size_w_h[0] = width;
//   size_w_h[0] = height;
//   return size_w_h;
//}


boxesAndAcc* detect_box_and_acc(char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen, char** names,network* net, int w, int h, int c)//  now...
{

    boxesAndAcc* boxesAndAccInstall;
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    float nms=.45;

    image **alphabet = load_alphabet();


    while(1){
        if(filename){
            strncpy(input, filename, 256);
        } else {
            printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if(!input) return NULL;
            strtok(input, "\n");
        }
/*
	// 0000
        image im = load_image_color(input,0,0);
        mage sized = letterbox_image(im, net->w, net->h);
*/
	//0001
        //int * size_wh = png_size(fopen(input, "rb+"));
	//int width = size_wh[0];
	//int height = size_wh[1];

	unsigned char * bytes = matToBytes(input, c);

        image im = bytesToMat2image(bytes, w, h);

        image sized = letterbox_image(im, net->w, net->h);
	//printf("height: %d.\n", h);

        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n-1];


        float *X = sized.data;
        time=what_time_is_it_now();
        network_predict(net, X);
        printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now()-time);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        //printf("%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms) do_nms_sort(dets, nboxes, l.classes, nms);

	boxesAndAccInstall = malloc(sizeof(boxesAndAcc)*nboxes);
	int* validBox = malloc(sizeof(int)*nboxes);

    	for(int i = 0; i < nboxes; ++i){
		validBox[i]=0;
		boxesAndAccInstall[i].isVaild = false;
		char labelstr[4096] = {0};
		int class = -1;
		float acc = 0.0;
		for(int j = 0; j < l.classes; ++j){

		    if (dets[i].prob[j] > thresh){
			validBox[i]++;
		        if (class < 0) {
		            strcat(labelstr, names[j]);
		            class = j;
		        } else {
		            strcat(labelstr, ", ");
		            strcat(labelstr, names[j]);
		        }
	//	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
			boxesAndAccInstall[i].isVaild = true;
			if(acc<dets[i].prob[j]*100){
				acc = dets[i].prob[j]*100;
				boxesAndAccInstall[i].acc = acc;
				boxesAndAccInstall[i].names = names[j];
			}
		    }

		}
		if(validBox[i]>0){
			boxesAndAccInstall[i].boxes = dets[i].bbox;
			boxesAndAccInstall[i].size = nboxes;
		}

	}


	free(validBox);
//	printf("%d\n", nboxes);
        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
        free_detections(dets, nboxes);
//	printf("%d\n", nboxes);
      if(outfile){
            save_image(im, outfile);
        }
        else{
            save_image(im, "predictions");
#ifdef OPENCV
            make_window("predictions", 512, 512, 0);
            show_image(im, "predictions", 0);
#endif
        }

        free_image(im);
        free_image(sized);
        if (filename) break;
    }
    return boxesAndAccInstall;
}



boxesAndAcc* test_detect_box_and_acc(char *datacfg, char *cfgfile, char *weightfile, char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen)
{
    list *options = read_data_cfg(datacfg);
    char *name_list = option_find_str(options, "names", "data/names.list");
    char **names = get_labels(name_list);
    boxesAndAcc* boxesAndAccInstall;
//    image **alphabet = load_alphabet();
    network *net = load_network(cfgfile, weightfile, 0);
    set_batch_network(net, 1);
    srand(2222222);
    double time;
    char buff[256];
    char *input = buff;
    float nms=.45;

    while(1){
        if(filename){
            strncpy(input, filename, 256);
        } else {
            printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if(!input) return NULL;
            strtok(input, "\n");
        }
        image im = load_image_color(input,0,0);
        image sized = letterbox_image(im, net->w, net->h);
        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n-1];


        float *X = sized.data;
        time=what_time_is_it_now();
        network_predict(net, X);
        printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now()-time);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
        //printf("%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms) do_nms_sort(dets, nboxes, l.classes, nms);

	boxesAndAccInstall = malloc(sizeof(boxesAndAcc)*nboxes);
	int* validBox = malloc(sizeof(int)*nboxes);

    	for(int i = 0; i < nboxes; ++i){
		validBox[i]=0;
		boxesAndAccInstall[i].isVaild = false;
		char labelstr[4096] = {0};
		int class = -1;
		float acc = 0.0;
		for(int j = 0; j < l.classes; ++j){

		    if (dets[i].prob[j] > thresh){
			validBox[i]++;
		        if (class < 0) {
		            strcat(labelstr, names[j]);
		            class = j;
		        } else {
		            strcat(labelstr, ", ");
		            strcat(labelstr, names[j]);
		        }
	//	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
			boxesAndAccInstall[i].isVaild = true;
			if(acc<dets[i].prob[j]*100){
				acc = dets[i].prob[j]*100;
				boxesAndAccInstall[i].acc = acc;
				boxesAndAccInstall[i].names = names[j];
			}
		    }

		}
		if(validBox[i]>0){
			boxesAndAccInstall[i].boxes = dets[i].bbox;
			boxesAndAccInstall[i].size = nboxes;
		}

	}


	free(validBox);
//	printf("%d\n", nboxes);
//        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
//        free_detections(dets, nboxes);
//	printf("%d\n", nboxes);
/*       if(outfile){
            save_image(im, outfile);
        }
        else{
            save_image(im, "predictions");
#ifdef OPENCV
            make_window("predictions", 512, 512, 0);
            show_image(im, "predictions", 0);
#endif
        }*/

        free_image(im);
        free_image(sized);
        if (filename) break;
    }
    return boxesAndAccInstall;

}




boxesAndAcc* detectByInputBuffer(float* buffer,float thresh, float hier_thresh, char *outfile, int fullscreen, char** names,network* net,int w,int h,int c)
{
//CPU for default, gpu_index = 1 for GPU.

   //printf("GPU index: %d....\n", gpu_index);

   //if(gpu_index>=0){
   //   	cuda_set_device(gpu_index);
   //}
#ifndef GPU
    gpu_index = -1;
#else
    if(gpu_index >= 0){
        cuda_set_device(gpu_index);
    }
#endif
    //net->gpu_index = gpu_index;
    boxesAndAcc* boxesAndAccInstall;
    srand(2222222);
    double time;
//   char buff[256];
//    char *input = buff;

	//printf("sizeof buffer: %ld\n", sizeof(buffer));
//printf("buffer 7? %lf\n", buffer[7]);
//printf("buffer 8? %lf\n", buffer[8]);
//printf("buffer 9? %lf\n", buffer[9]);

     image **alphabet = load_alphabet();


    float nms=.45;
    if(1){

//        image im = load_image_color(input,0,0);
	//printf("buffer:%f\n",buffer[0]);
	image im = load_float_and_whc(buffer,w,h,c);
        //printf("img:%f\n",im.data[0]);
        image sized = letterbox_image(im, net->w, net->h);
        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        layer l = net->layers[net->n-1];


        float *X = sized.data;
/*	for(int i=0;i<608*608*3;i++){
		printf("%f\n",X[i],i);
	}
*/
	printf("w:%d,h:%d,c:%d\n",im.w,im.h,im.c);
        time=what_time_is_it_now();
        network_predict(net, X);
        printf("Predicted in %f seconds.\n", what_time_is_it_now()-time);
        int nboxes = 0;
        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
//        printf("boxes:%d\n", nboxes);
        //if (nms) do_nms_obj(boxes, probs, l.w*l.h*l.n, l.classes, nms);
        if (nms) do_nms_sort(dets, nboxes, l.classes, nms);

	boxesAndAccInstall = malloc(sizeof(boxesAndAcc)*nboxes);
	int* validBox = malloc(sizeof(int)*nboxes);

    	for(int i = 0; i < nboxes; ++i){
		validBox[i]=0;
		boxesAndAccInstall[i].isVaild = false;
		char labelstr[4096] = {0};
		int class = -1;
		float acc = 0.0;
		for(int j = 0; j < l.classes; ++j){

		    if (dets[i].prob[j] > thresh){
			validBox[i]++;
		        if (class < 0) {
		            strcat(labelstr, names[j]);
		            class = j;
		        } else {
		            strcat(labelstr, ", ");
		            strcat(labelstr, names[j]);
		        }
	//	        printf("%s: %.0f%%\n", names[j], dets[i].prob[j]*100);
			boxesAndAccInstall[i].isVaild = true;
			if(acc<dets[i].prob[j]*100){
				acc = dets[i].prob[j]*100;
				boxesAndAccInstall[i].acc = acc;
				boxesAndAccInstall[i].names = names[j];
			}
		    }

		}
		if(validBox[i]>0){
			boxesAndAccInstall[i].boxes = dets[i].bbox;
			boxesAndAccInstall[i].size = nboxes;
		}

	}


	free(validBox);
//	printf("%d\n", nboxes);
        draw_detections(im, dets, nboxes, thresh, names, alphabet, l.classes);
        free_detections(dets, nboxes);
//	printf("%d\n", nboxes);
      if(outfile){
            save_image(im, outfile);
        }
        else{
            save_image(im, "predictions");
#ifdef OPENCV
	    printf("I am OPENCV...\n");
            make_window("predictions", 512, 512, 0);
            show_image(im, "predictions", 0);
#endif
        }

        free_image(im);
        free_image(sized);

    }
    return boxesAndAccInstall;
}


//   jpg to bytes...
unsigned char * jpg2BytesInC(char *filename, int channels)
{

	unsigned char * bytes = matToBytes(filename, channels);
        //printf("InC, bytes[1]: %d\n", bytes[1]);
	return bytes;

}


// detect by bytes...
char * detectByInputBytes1111(unsigned char * bytes, float thresh, float hier_thresh, char *outfile, int fullscreen, char** names,network* net, int w, int h, int c, char *labelpath ,char *datacfg, int top)//  now...
{
//    printf("detects beginning");
    list *options = read_data_cfg(datacfg);
    printf("1");
    char *name_list = option_find_str(options, "names", 0);
    if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
    printf("2");
//        if(!name_list) name_list = option_find_str(options, "labels", "data/labels.list");
    if(top == 0) top = option_find_int(options, "top", 1);
    //printf("****************************detectByInputBytes in cpp.\n");
//    boxesAndAcc* boxesAndAccInstall;
    srand(2222222);
//    printf("3");
    int *indexes = calloc(top, sizeof(int));
//    printf("4");
    char **namess = get_labels(name_list);
    int i = 0;
    double time;
    char buff[256];
    char *input = buff;
    float nms=.45;
    //类名
//    image **alphabet = load_alphabet(labelpath);
//    printf("5");

    while(1){

    //输入图片路径，没有用
//        if(path){
//            strncpy(input, path, 256);
//        }
//        else {
//            printf("Enter Image Path: ");
//            fflush(stdout);
//            input = fgets(input, 256, stdin);
//            if(!input) return NULL;
//            strtok(input, "\n");
////        }
//        printf("11");
/*
	// 0000
        image im = load_image_color(input,0,0);
        mage sized = letterbox_image(im, net->w, net->h);
*/
	//0001
        //int * size_wh = png_size(fopen(input, "rb+"));
	//int width = size_wh[0];
	//int height = size_wh[1];

	//unsigned char * bytes = matToBytes(input, c);
//加载图片
        image im = bytesToMat2image(bytes, w, h);
//        image im = load_image_color(input,0,0);
//图片裁剪
        image sized = letterbox_image(im, net->w, net->h);
//        printf("12");
	//printf("height: %d.\n", h);

        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
//        printf("13");
//        layer l = net->layers[net->n-1];
        //
//        float *X = sized.data;

        float *X = sized.data;
        time=what_time_is_it_now();
//        network_predict(net, X);
//output
        float *predictions = network_predict(net,X);
//        printf("14");
//add
        if(net->hierarchy) hierarchy_predictions(predictions, net->outputs, net->hierarchy, 1, 1);
//        printf("15");
        top_k(predictions, net->outputs, top, indexes);
//        printf("111");
        printf("Predicted in %f seconds.\n", what_time_is_it_now()-time);
//        printf("112");
//        int nboxes = 0;
//        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
//        for(i = 0; i < top; ++i){
//                    int index = indexes[i];
//                    //if(net->hierarchy) printf("%d, %s: %f, parent: %s \n",index, names[index], predictions[index], (net->hierarchy->parent[index] >= 0) ? names[net->hierarchy->parent[index]] : "Root");
//                    //else printf("%s: %f\n",names[index], predictions[index]);
//                    printf("%5.2f%%: %s\n", predictions[index]*100, names[index]);
//                }

         int index = indexes[0];
//         printf("113");
         //if(net->hierarchy) printf("%d, %s: %f, parent: %s \n",index, names[index], predictions[index], (net->hierarchy->parent[index] >= 0) ? names[net->hierarchy->parent[index]] : "Root");
         //else printf("%s: %f\n",names[index], predictions[index]);
         printf("%5.2f%%: %s\n", predictions[index]*100, namess[index]);
//         printf("114");

        if(sized.data != im.data) free_image(sized);
                free_image(im);
//                if (path) break;

        printf("detect over!");
       return namess[index];
//return "";
}
}

