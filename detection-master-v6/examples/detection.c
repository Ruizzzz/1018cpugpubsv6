#include "detection.h"
#include "image.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>


//extern void test_detector(char *datacfg, char *cfgfile, char *weightfile, char *filename, float thresh, float hier_thresh, char *outfile, int fullscreen);
network * load_network_test11(char *cfgfile, char *weightfile,int batchsize, int gpu_index_params, char *gpuid)
{

    //printf("from java batchsize: %d\n", batchsize);
    //printf("from java gpuids: %s\n", gpuid);

    gpu_index = gpu_index_params;

    network *net = load_network(cfgfile, weightfile, 0);

    set_batch_network(net, 1);
    //set_batch_network(net, batchsize);
    //printf("actual net batchsize: %d\n", net->batch);

    return net;
}















// detect by bytes...
char * detectByInputBytes11(network* net, char *datacfg, int top)//  now...
{

    printf("detects beginning");
    list *options = read_data_cfg(datacfg);
    printf("1");
    char *name_list = option_find_str(options, "names", 0);
    if(!name_list) name_list = option_find_str(options, "labels", "/root/map123/mlr/darknet/data/labels.list");
    printf("2");
    if(!name_list) name_list = option_find_str(options, "labels", "data/labels.list");
    if(top == 0) top = option_find_int(options, "top", 1);
    //printf("****************************detectByInputBytes in cpp.\n");
    char **names = get_labels(name_list);
    int *indexes = calloc(top, sizeof(int));
//    boxesAndAcc* boxesAndAccInstall;
    srand(2222222);
    printf("3");
    printf("4");
//    char **names = get_labels(name_list);
    int i = 0;
    double time;
    char buff[256];
    char *input = buff;
    float nms=.45;
    //类名
//    image **alphabet = load_alphabet(labelpath);
    printf("5");

    while(1){
//        if(outfile){
//            strncpy(input, outfile, 256);
//        } else {
            printf("Enter Image Path: ");
            fflush(stdout);
            input = fgets(input, 256, stdin);
            if(!input) return NULL;
            strtok(input, "\n");
//        }
        printf("11");
/*
	// 0000
        image im = load_image_color(input,0,0);
        mage sized = letterbox_image(im, net->w, net->h);
*/
	//0001
        //int * size_wh = png_size(fopen(input, "rb+"));
	//int width = size_wh[0];
	//int height = size_wh[1];

	//unsigned char * bytes = matToBytes(input, c);
//加载图片
//        image im = bytesToMat2image(input, 0, 0);
        image im = load_image_color(input , 0 ,0);
//图片裁剪
        image sized = letterbox_image(im, net->w, net->h);
        printf("12");
	//printf("height: %d.\n", h);

        //image sized = resize_image(im, net->w, net->h);
        //image sized2 = resize_max(im, net->w);
        //image sized = crop_image(sized2, -((net->w - sized2.w)/2), -((net->h - sized2.h)/2), net->w, net->h);
        //resize_network(net, sized.w, sized.h);
        printf("13");
//        layer l = net->layers[net->n-1];
        //
//        float *X = sized.data;

        float *X = sized.data;
        time=what_time_is_it_now();
//        network_predict(net, X);
//output
        float *predictions = network_predict(net,X);
        printf("14");
//add
        if(net->hierarchy) hierarchy_predictions(predictions, net->outputs, net->hierarchy, 1, 1);
        printf("15");
        top_k(predictions, net->outputs, top, indexes);
        printf("111");
        printf("%s: Predicted in %f seconds.\n", input, what_time_is_it_now()-time);
        printf("112");
//        int nboxes = 0;
//        detection *dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, 0, 1, &nboxes);
//        for(i = 0; i < top; ++i){
//                    int index = indexes[i];
//                    //if(net->hierarchy) printf("%d, %s: %f, parent: %s \n",index, names[index], predictions[index], (net->hierarchy->parent[index] >= 0) ? names[net->hierarchy->parent[index]] : "Root");
//                    //else printf("%s: %f\n",names[index], predictions[index]);
//                    printf("%5.2f%%: %s\n", predictions[index]*100, names[index]);
//                }

         int index = indexes[0];
         printf("113");
         //if(net->hierarchy) printf("%d, %s: %f, parent: %s \n",index, names[index], predictions[index], (net->hierarchy->parent[index] >= 0) ? names[net->hierarchy->parent[index]] : "Root");
         //else printf("%s: %f\n",names[index], predictions[index]);
         printf("%5.2f%%: %s\n", predictions[index]*100, names[index]);
         printf("114");

        if(sized.data != im.data) free_image(sized);
                free_image(im);
//                if (outfile) break;

        printf("detect over!");
         return names[index];
}
}

typedef unsigned char byte;

int main(int argc, char **argv)
{
//    //test_resize("data/bad.jpg");
//    //test_box();
//    //test_convolutional_layer();
//    if(argc < 2){
//        fprintf(stderr, "usage: %s <function>\n", argv[0]);
//        return 0;
//    }
//    gpu_index = find_int_arg(argc, argv, "-i", 0);
//    if(find_arg(argc, argv, "-nogpu")) {
//        gpu_index = -1;
//    }
//
//#ifndef GPU
//    gpu_index = -1;
//#else
//    if(gpu_index >= 0){
//        cuda_set_device(gpu_index);
//    }
//#endif
//
//
//    float thresh = find_float_arg(argc, argv, "-thresh", .5);
////    char *filename = (argc > 3) ? argv[3]: 0;
//    char *outfile = find_char_arg(argc, argv, "-out", 0);
//    int fullscreen = find_arg(argc, argv, "-fullscreen");
//
//    char **names = load_names("cfg/coco.data");
//    network *net = load_network_test(argv[1], argv[2],0,0,0);
////    printf("%s\n",names[0]);
// //   load_network_test(argv[1], argv[2],net);
//    float hier_thresh =0.5;
//
//    int w=768;
//    int h=576;
//    int c=3;
//
////file
//    FILE *fp = NULL;
//    fp=fopen("./dog_.txt","r");
//    if(!fp) exit(0);
//    float *data =(float*)malloc(sizeof(float)*w*h*c);
//
//    //unsigned char* data =(unsigned char*)malloc(sizeof(unsigned char)*w*h*c);
//
//    for (int i=0;i<w*h*c;i++){
//	fscanf(fp,"%e",(data+i));
//    }
//    fclose(fp);
//
//
///*   for (int i=0;i<w*h*c;i++){
//	printf("%f\n",data[i]);
//    }
//*/
//
//    boxesAndAcc* bTest = detectByInputBuffer(data, thresh, hier_thresh, outfile, fullscreen,names,net,w,h,c);
////    boxesAndAcc* bTest = detect_box_and_acc( filename,data, thresh, hier_thresh, outfile, fullscreen,names,net,w,h,c);
//
////    boxesAndAcc* bTest = test_detect_box_and_acc_1("cfg/coco.data", argv[1], argv[2], filename, thresh, 0.5, outfile, fullscreen,net);
////    boxesAndAcc* bTest = test_detect_box_and_acc("cfg/coco.data", argv[1], argv[2], filename, thresh, .5, outfile, fullscreen,net);
//    if(bTest!=NULL){
//	int size = bTest[0].size;
//	for (int i=0;i<size;i++){
//		if(bTest[i].isVaild){
//			printf("name:%s:acc:%f\n",bTest[i].names,bTest[i].acc);
//			box boxInstance = bTest[i].boxes;;
//			printf("x:%f,y:%f,w:%f,h:%f\n",boxInstance.x,boxInstance.y,boxInstance.w,boxInstance.h);
//		}
//	}
//	free(bTest);
//    }else{
//	printf("bTest is NULL\n");
//    }
//    free(data);
//    return 0;

char *cfgfile="/root/map123/mlr/darknet-yolov3/cfg/resnet50.cfg";
    char *weightfile="/root/map123/mlr/darknet-yolov3/resnet50.weights";
    char *datacfg="/root/map123/mlr/darknet/cfg/imagenet1k.data";
    network *net=load_network(cfgfile,weightfile,0);
    set_batch_network(net,1);
    detectByInputBytes11(net,datacfg,5);
}

